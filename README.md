# Standard Development VM

## Purpose
This "standard" development virtual machine was created in order to allow our devs to code in a unified environment. This can alleviate differences in local environments where some experience issues while others report that things run as-is.
Benefits include:

  - Cross platform. This VM can be run on most operating systems.
  - Standardized environment. Ensures that all devs have the same software packages installed.
  - Free. This VM uses only free/freely-available software.

## Requirements
The following dependencies must be met before starting. Since the host (your computer's) operating system can be any of a number of choices, these are up to you to install and configure. They should be pretty straightforward, and defaults should work fine.
  - [VirtualBox] with guest additions
  - [Vagrant]
  - vagrant-vbguest (strongly recommended)-- "vagrant plugin install vagrant-vbguest"
  - vagrant-proxyconf (optional, see "Advanced Usage") -- "vagrant plugin install vagrant-proxyconf"

## Basic Usage

1. Clone or download the repository
2. Open command prompt or terminal in the directory that the repo was cloned or extracted to. It should be the location including Vagrantfile
3. Start the VM with "vagrant up"
4. Wait for the VM to fully initialize. This will be when the prompt returns. **NOTE** - This may take a while the first time you run this command, as there is a lot going on behind the scenes.
5. Use your favorite SSH client to connect to "127.0.0.1:2222" and log in.
6. You are now ready to get busy with coding.

## Advanced Usage

vagrant-proxyconf allows you to "cache" debian's apt repository. This will significantly decrease subsequent provisioning of future machines. To use this module, download the debian "jessie" 64-bit appliance from [osboxes]. Once imported and started, follow these [instructions for apt-cacher-ng]. It is recommended you set the network adapter to bridged, and configure a static ip. Then edit the Vagrantfile and change "config.apt_proxy.http" to the ip of the apt-caching VM.

[//]: # (reference links)

   [VirtualBox]: <https://www.virtualbox.org/wiki/Downloads>
   [Vagrant]: <https://www.vagrantup.com/downloads.html>
   [osboxes]: <https://sourceforge.net/projects/osboxes/files/vms/vbox/Debian/8.4.0/Debian_8.4.0-64bit.7z/download>
   [instructions for apt-cacher-ng]: <https://www.jamescoyle.net/tag/apt-cacher-ng>

